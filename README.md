# JavaScript 语言基础知识总结(思维导图) #

转自: [七月、前端工程师王子墨](http://julying.com/blog/the-features-of-javascript-language-summary-maps/)

## 内容 ##
 - [JavaScript数组](https://bitbucket.org/theReturnOfTheMonk/js-mind-map/src/683e2e68098434c3b2f7e5538e656e487d800447/images/JavaScript-array.gif?at=master)
 - [JavaScript函数基础](https://bitbucket.org/theReturnOfTheMonk/js-mind-map/src/683e2e68098434c3b2f7e5538e656e487d800447/images/JavaScript-function-base.gif?at=master)
 - [JavaScript运算符](https://bitbucket.org/theReturnOfTheMonk/js-mind-map/src/683e2e68098434c3b2f7e5538e656e487d800447/images/Javascript-operational-character.gif?at=master)
 - [JavaScript流程控制](https://bitbucket.org/theReturnOfTheMonk/js-mind-map/src/683e2e68098434c3b2f7e5538e656e487d800447/images/JavaScript-process-statement.gif?at=master)
 - [JavaScript正则表达式](https://bitbucket.org/theReturnOfTheMonk/js-mind-map/src/683e2e68098434c3b2f7e5538e656e487d800447/images/JavaScript-regular-expressions.gif?at=master)
 - [JavaScript字符串函数](https://bitbucket.org/theReturnOfTheMonk/js-mind-map/src/683e2e68098434c3b2f7e5538e656e487d800447/images/JavaScript-string-function.gif?at=master)
 - [JavaScript数据类型](https://bitbucket.org/theReturnOfTheMonk/js-mind-map/src/683e2e68098434c3b2f7e5538e656e487d800447/images/The-JavaScript-data-type-1.gif?at=master)
 - [JavaScript变量](https://bitbucket.org/theReturnOfTheMonk/js-mind-map/src/683e2e68098434c3b2f7e5538e656e487d800447/images/The-JavaScript-variable.gif?at=master)
 - [Window对象](https://bitbucket.org/theReturnOfTheMonk/js-mind-map/src/683e2e68098434c3b2f7e5538e656e487d800447/images/Window-object.gif?at=master)
 - [DOM基本操作](https://bitbucket.org/theReturnOfTheMonk/js-mind-map/src/683e2e68098434c3b2f7e5538e656e487d800447/images/DOM-operation.gif)

## TODO ##
~~加图~~
